configfile: "config.yaml"

import glob
nNT= len(glob.glob(config["annodir"]+"nttype*_annodata.txt"))
Fvinfermodes = {
'MLE':	config["outputdir"] + "{tumorname}/{tumorname}_parm.Rdata",
'MLE-ASH': config["outputdir"] + "parmASHmean.Rdata",
'none': config["paramdir"] + "parmASHmean.Rdata" }

rule all:
	input: [config["outputdir"]+"{tumorname}/{tumorname}_BayesFactorFDR.txt".format(tumorname=tumorname) for tumorname in config['tumornames']]


rule prepdata:
	input:
		anno=glob.glob(config["annodir"]+"nttype*_annodata.txt"),
                mut=config["mutationfile"]
	output:
		config["outputdir"] + "{tumorname}/data_ycol/{tumorname}_nttype1_y.txt"
	params:
		nNT=nNT
	log:	
		config["logdir"] + "{tumorname}/{tumorname}_prepdata.log"
	script: 
		"scripts/Py_run_prepdata.py" 

rule BMRinfer:
	input:
		config["outputdir"] + "{tumorname}/data_ycol/{tumorname}_nttype1_y.txt"
	output:
		config["outputdir"] + "{tumorname}/{tumorname}_parameters_BMvar.Rdata"
	params:
		nNT=nNT
	log:	
		config["logdir"] + "{tumorname}/{tumorname}_BMRinfer.log"
	script: 
		"scripts/R_run_BMR_model.R" 

rule Funcvinfer:
	input:
		config["outputdir"] + "{tumorname}/{tumorname}_parameters_BMvar.Rdata"	
	output:
		config["outputdir"] + "{tumorname}/{tumorname}_parm.Rdata"
	params:
		nNT=nNT
	log:	
		config["logdir"] + "{tumorname}/{tumorname}_funcvinfer.log"
	script: 
		"scripts/R_run_funcv_model.R" 

rule ASHfuncv:
	input: [config["outputdir"]+"{tumorname}/{tumorname}_parm.Rdata".format(tumorname=tumorname) for tumorname in config['tumornames']]
	output: 
		config["outputdir"] + "parmASHmean.Rdata"
	params:
		nNT=nNT
	log:	
		config["logdir"] + "ASHfuncv.log"
	script: 
		"scripts/R_run_multiT_ASH.R"

rule HMMinfer:
	input:
		BMM = config["outputdir"] + "{tumorname}/{tumorname}_parameters_BMvar.Rdata",
		funcv = Fvinfermodes[config["Funcvinfer"]]
	output:
		config["outputdir"] + "{tumorname}/{tumorname}_hmmOG.rds"
	params:
		nNT=nNT
	log:	
		config["logdir"] + "{tumorname}/{tumorname}_HMMinfer.log"
	script: 
		"scripts/R_run_HMM.R"

rule BayesFactor:
	input:
		BMM = config["outputdir"] + "{tumorname}/{tumorname}_parameters_BMvar.Rdata",
		funcv = Fvinfermodes[config["Funcvinfer"]],
		hmm = rules.HMMinfer.output if config["HMMinfer"] else config["paramdir"] + "hmmOGpar_ASHmean.rds"
	output:
		config["outputdir"] + "{tumorname}/{tumorname}_BayesFactor.txt"
	params:
		nNT=nNT
	threads:
		100
	log:	
		config["logdir"] + "{tumorname}/{tumorname}_BayesFactor.log"
	script: 
		"scripts/R_run_predict_BF_HMM.R" 

rule addFDR:
	input:
		config["outputdir"] + "{tumorname}/{tumorname}_BayesFactor.txt"	
	output:
		config["outputdir"] + "{tumorname}/{tumorname}_BayesFactorFDR.txt"
	params:
		nNT=nNT
	log:	
		config["logdir"] + "{tumorname}/{tumorname}_addFDR.log"
	script: 
		"scripts/R_run_multiT_FDR_siggene.R" 


rule test:
	input:
		BMM = config["outputdir"] + "{tumorname}/{tumorname}_parameters_BMvar.Rdata",
		funcv = config["paramdir"] + "parmASHmean.Rdata"
	output:
		config["outputdir"] + "{tumorname}/test.Rd"
	params:
		nNT=nNT
	log:	
		config["logdir"] + "test.log"
	script: 
		"scripts/test.R" 

