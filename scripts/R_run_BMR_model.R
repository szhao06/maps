source(snakemake@config[["runconfig"]])

log <- file(snakemake@log[[1]], open="wt")
sink(log)
sink(log, type="message")

print("Running R_run_BMR_model.R: tyring to infer parameters for Background mutation rate model (BMM) parameters.")
print(paste0("Starting at ", Sys.time()))
save.image("temp.Rd")
# only using silent mutation to see if it can capture gene specific mutation rate.
Matrixlist <- readmodeldata(Afileinfo, Yfileinfo, BMvars, BMmuttype, Readinvars, Qnvars, functypecodelevel = NULL,qnvarimpute=c(NA,NA))
betabaseline0 <-log(apply(rbind(unlist(lapply(lapply(Matrixlist,'[[',2), colMeans)),
1/unlist(lapply(lapply(Matrixlist,'[[',2), nrow))),2,max))
nbeta <- dim(Matrixlist[[1]][[1]])[2] -1
initpars <- c(betabaseline0,rep(Beta0, nbeta), 0 ,Alpha0)
fixstatus <- c(rep(T, Totalnttype), rep(F, nbeta), T, Alphainferstatus)
Y_g_s_0 <- data.table(agg_var = character(), y = numeric(), key = "agg_var")
Mu_g_s_0 <- data.table(agg_var = character(), V1 = numeric(), key = "agg_var")
BMpars <- optifix(initpars, fixstatus, loglikfn, matrixlist= Matrixlist, y_g_s_in=Y_g_s_0, mu_g_s_in=Mu_g_s_0, method = "BFGS", control=list(trace=6, fnscale=-1), hessian=T)
names(BMpars$fullpars) <- c(paste("nttype", 1:Totalnttype, sep=""), colnames(Matrixlist[[1]][[1]])[-1], "beta_f0", "alpha")

print("BMR Inference done ...")

BMparsfile <- paste(Outputdir,"/", Ttype,"_parameters_BMvar.Rdata",sep="")
save(BMpars, file = BMparsfile) # save as .Rd
OutBMparsfile <- paste(Outputdir,"/", Ttype,"_parameters_BMvar.txt",sep="")
OutBMpars <- .parfunc(BMpars)
rownames(OutBMpars) <- c(Ttype, Ttype)
write.table(OutBMpars,row.names=T, col.names=F,file=OutBMparsfile,sep="\t", quote = F) # save as .txt

Y_g_s_all <- gene_y(Matrixlist)
Vbeta_s <- BMpars$fullpars[1: length(BMpars$fullpars)-1]
Mu_g_s_all <- gene_mu(Vbeta_s, Matrixlist)
Y_mu_gfile <- paste(Outputdir,"/", Ttype,"_BM_y_mu_g.Rdata",sep="")
save(Y_g_s_all, Mu_g_s_all, file = Y_mu_gfile)

# gsQC(BMpars, Matrixlist, Outputdir, Ttype) # get lambda distribution plots

print("Writing output done...")
print(paste0("Finished BMR inference at ", Sys.time()))


