import subprocess,os,glob,sys
import pandas as pd
import re
import time
import logging
import logging.handlers as handlers
import pathlib
import pdb

logfile = snakemake.log[0]
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logHandler = handlers.RotatingFileHandler(logfile, maxBytes=500, backupCount=0)
logHandler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)

annovar = snakemake.config["scriptdir"] + "table_annovar.pl"
annofiles = glob.glob(snakemake.config["annodir"] + "nttype?_annodata.txt")
mutfile = snakemake.input["mut"]
outputdir = snakemake.config["outputdir"] + snakemake.wildcards["tumorname"] + "/data_ycol/"
pathlib.Path(outputdir).mkdir(parents=True, exist_ok=True) 
outputname = snakemake.wildcards["tumorname"] + "_nttype{nt}_y.txt"
gdbfilename = snakemake.wildcards["tumorname"] +"_gdb.txt"
gdbfile = outputdir + gdbfilename 

def maftogdb(maffile, gdbfile):
    df = pd.read_csv(maffile, sep="\t", dtype=str) 
    df.drop_duplicates(subset=None, inplace=True)
    df1 = df.groupby(['Chromosome', 'Position', 'Ref', 'Alt'], as_index=False).count()
    df1.insert(2,'Position2',df1['Position'])
    df1.to_csv(gdbfile, sep="\t", header=False, index=False)

def main():
    maftogdb(mutfile,gdbfile)
    ANNOCMD = "{annovar} {infile} {gdbdir} -buildver hg19 -out {outputdir}{outnamebase} -remove -protocol generic -operation f -genericdbfile {mutgdbfile}  -nastring 0"
    for annofile in annofiles:
        nt = re.search('nttype(\d+)_', annofile).groups()[0]
        cmd1 = ANNOCMD.format(annovar=annovar, infile=annofile, gdbdir = outputdir, outputdir = outputdir, outnamebase = outputname.format(nt=nt), mutgdbfile= gdbfilename)
        logger.info("starting annovar...")
        subprocess.call(cmd1, shell=True)
        CUTCMD = r"cut -f 6 " + "{outputdir}{outputname}.hg19_multianno.txt > {outputdir}{outputname}.temp".format(outputdir= outputdir, outputname=outputname.format(nt=nt))
        subprocess.call(CUTCMD, shell=True)
        SEDCMD = r"sed '1s/generic/y/;2d' {outputdir}{outputname}.temp > {outputdir}{outputname}.temp2".format(outputdir= outputdir, outputname=outputname.format(nt=nt))
        subprocess.call(SEDCMD, shell=True)
        ADDGCMD = "paste {outputdir}{outputname}.temp2 {annofile}| cut -f 1,7 > {outputdir}{outputname}; rm {outputdir}{outputname}.*".format(outputdir= outputdir, outputname=outputname.format(nt=nt),annofile=annofile)
        subprocess.call(ADDGCMD, shell=True)
        logger.info("starting process y...")
        logger.info("done")

if __name__ == "__main__":
    main()
