library(tidyverse)
#Obtain annotation files from data directory
annotation_files <- dir("../data",pattern="nttype",full.names=T)

read_anno_genes <- function(annof){
    #read gene name and 
    read_delim(annof,delim="\t", col_types=cols_only(genename='c',nttypecode='i')) %>% distinct()
}

anno_df <- map_df(annotation_files,read_anno)
good_gene_df <- filter(anno_df,!grepl("[^a-zA-Z0-9]",genename))
n_anno_df <- group_by(good_gene_df,genename) %>% summarise(n_genes=n()) %>% inner_join(anno_df) %>% arrange(n_genes)
genelist <- filter(n_anno_df,n_genes==max(n_genes)) %>% distinct(genename)
set.seed(3587)
subset_genelist  <-  sample_n(genelist,200,replace=F)


(new_annotation_files <- gsub("/data/","/sample_data/",annotation_files))

sub_anno_genes <- function(annof,new_anno_f,subset_genes){
    #read gene name and 
    read_delim(annof,delim="\t") %>% semi_join(subset_genes) %>% write_delim(new_anno_f,delim="\t")
}
walk2(annotation_files,new_annotation_files,sub_anno_genes,subset_genes=subset_genelist)




