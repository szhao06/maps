library(Hmisc)
#devtools::install_github("stephens999/ashr",ref="master")
#devtools::install_github("stephens999/ashr", ref="general")

ASH_par <- function(parm, tlist, vlist, outputdirbase){
  nttype <- dim(parm)[1]/6
  nv <- dim(parm)[2]
  pdf(paste(outputdirbase,"funcv_ASH_summary.pdf", sep=""), width=16, height=3.5*nv)
  par(mfrow=c(nv,6))
  outparlist <- list()
  for (v in 1:nv){
    templist <-list()
    for (m in c(1,3,5)) {
      print(c(v,m))
      x <- parm[seq(m,(nttype-1)*6+m,6),v]
      sd <- parm[seq(m+1,(nttype-1)*6+m+1,6),v]
      x.ash <- ashr::ash(x,sd,mode="estimate") 
      out <- list(pars=x,pars.ash = x.ash$result$PosteriorMean)
      templist[[(m+1)/2]]<-x.ash$result$PosteriorMean
      plot(do.call(cbind,out),bg='tomato2', pch=21)
      abline(a=0,b=1,lty=2)
      boxplot(out, outpch = NA) 
      stripchart(out,vertical = TRUE, method = "jitter", 
                 pch = 21, col =c("maroon", "orange"), bg = "bisque", add = TRUE)
    }
    outparlist[[v]] <- do.call(rbind, templist)
  }
  dev.off()
  parall<- do.call(rbind,outparlist)
  parall <- data.frame(parall)
  colnames(parall)<- tlist
  rownames(parall)<- paste(rep(vlist,each=3),c("TSG","OC","nc"))
  write.table(parall, row.names=T, col.names=T,file=paste(outputdirbase,"combined_funcv_ASH.txt",sep=""),sep="\t", quote = F)
  parmASHmean <- list()
  for (m in 1:3) {
    parmASHmean[[m]] <- rowMeans(parall[seq(m,(nv-1)*3+m,3),])
  }
  save(parmASHmean, file= paste(outputdirbase,"parmASHmean.Rdata",sep=""))
  
  # for (i in 1:length(tlist)){
  #   ttype <- tlist[i]
  #   outputfile <- paste(outputdirbase,"/", ttype, "/", ttype,"_parameters_","_funcv-ASH.Rdata",sep="")
  #   tpar <- list(parall[(1:nv)*3-2,i], parall[(1:nv)*3-1,i],parall[(1:nv)*3,i])
  #   save(tpar, file=outputfile)
  #   }
}

ASH_par2 <- function(parm, tlist, vlist, outputdirbase){
  nttype <- dim(parm)[1]/4
  nv <- dim(parm)[2]
  pdf(paste(outputdirbase,"funcv_ASH_summary.pdf", sep=""), width=16, height=3.5*nv)
  par(mfrow=c(nv,4))
  outparlist <- list()
  for (v in 1:nv){
    templist <-list()
    for (m in c(1,3)) {
      print(c(v,m))
      x <- parm[seq(m,(nttype-1)*4+m,4),v]
      sd <- parm[seq(m+1,(nttype-1)*4+m+1,4),v]
      x.ash <- ashr::ash(x,sd,mode="estimate") 
      #x.ash <- ashr::ash(x,sd)
      out <- list(pars=x,pars.ash = x.ash$result$PosteriorMean)
      templist[[(m+1)/2]]<-x.ash$result$PosteriorMean
      plot(do.call(cbind,out),bg='tomato2', pch=21)
      abline(a=0,b=1,lty=2)
      boxplot(out, outpch = NA) 
      stripchart(out,vertical = TRUE, method = "jitter", 
                 pch = 21, col =c("maroon", "orange"), bg = "bisque", add = TRUE)
    }
    outparlist[[v]] <- do.call(rbind, templist)
  }
  dev.off()
  parall<- do.call(rbind,outparlist)
  #colnames(parall)<- tlist
  rownames(parall)<- paste(rep(vlist,each=2),c("TSG","OC"))
  write.table(parall, row.names=T, col.names=T,file=paste(outputdirbase,"combined_funcv_ASH.txt",sep=""),sep="\t", quote = F)
  parmASHmean <- list()
  for (m in 1:2) {
    parmASHmean[[m]] <- rowMeans(parall[seq(m,(nv-1)*2+m,2),])
    names(parmASHmean[[m]]) <- vlist
  }
  names(parmASHmean) <- c("TSG","OC")
  parmASHmean[["nc"]] <- rep(0, length(parmASHmean[["TSG"]]))
  names(parmASHmean[["nc"]])  <-  vlist       
  
  save(parmASHmean, file= paste(outputdirbase,"parmASHmean.Rdata",sep=""))
}