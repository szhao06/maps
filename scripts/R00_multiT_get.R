library(Hmisc)

getloglik<- function(tlist, BFtype="combined"){
  loglik.c <- list()
  loglik.nc <- list()
  genelist <- list()
  for (i in 1:length(tlist)){
    ttype <- tlist[i]
    gdffile <- paste0(Outputdirbase,"/", ttype,"/", ttype,"_BayesFactor.txt",sep="")
    gdf <- read.table(gdffile, header=T, stringsAsFactors = F)
    gdf.sorted <- gdf[order(gdf$gene),]
    if (BFtype =="TSG"){
      BFtemp <- gdf.sorted$TSGBF
    } else if (BFtype =="OC"){
      BFtemp <- gdf.sorted$OCBF
    } else {
      BFtemp <- gdf.sorted$BF
    }
    if (length(BFtemp[which(is.na(BFtemp))]) >0){
      print(paste("in ", ttype, "the following genes' BF are na: they have been changed to 50"))
      print(cbind(gdf.sorted$gene[which(is.na(BFtemp))],BFtemp[which(is.na(BFtemp))]))
      BFtemp[which(is.na(BFtemp))] <- 50
    }
    if (length(BFtemp[which(is.infinite(BFtemp))]) >0){
      print(paste("in", ttype, "the following genes' BF are inf: they have been changed to 50"))
      print(cbind(gdf.sorted$gene[which(is.infinite(BFtemp))],BFtemp[which(is.infinite(BFtemp))]))
      BFtemp[which(is.infinite(BFtemp))] <- 50
    }
    loglik.c[[i]] <- BFtemp
    loglik.nc[[i]] <- rep(0,length(gdf.sorted$BF))
    genelist[[i]] <- gdf.sorted$gene
  }
  if (length(unique(genelist))==1){
    return(list(loglik.nc, loglik.c, genelist[[1]]))
    }
  else{ # TODO need to fix this, maybe use smartbind
    warning("input gdf not matching\n")
    return(list(loglik.nc, loglik.c, genelist))
  }
}

getpar <- function(outputdirbase, tlist){
  parmlist <- list()
  for (i in 1:length(tlist)){
    ttype <- tlist[i]
    parmlist[[i]] <- read.table(paste(outputdirbase,"/", ttype, "/", ttype,"_parameters_funcv.txt",sep=""))
  }
  parm <- do.call(rbind,parmlist)
  parm <- parm[,2:dim(parm)[2]]
  return(parm)
}



